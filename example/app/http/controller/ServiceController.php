<?php 

namespace App\Http\Controller;

use IServe\Microservice\Common\Http\Traits\ConsumeExternalService;

class ServiceController {
	use ConsumeExternalService;

    /**
     * The base uri to consume feed service
     * @var string
     */
    public $baseUri;

    /**
     * Authorization secret to pass to feed api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('microservices.one.base_uri')
			.config('microservices.one.uri');
        $this->secret = config('microservices.one.secret');
    }
	
	public function allPost($request) 
	{
		return $this->performRequest('GET', 'example/all', null, [
			'User-Id' => $request->header('example-header')
		]);
	}
}