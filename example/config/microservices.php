<?php

return [
    'one'   =>  [
        'base_uri'  =>  env('SERVICE_BASE_URL'),
		'uri' => env('SERVICE_URI'),
        'secret'  =>  env('SERVICE_SECRET'),
    ],

];