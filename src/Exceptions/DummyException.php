<?php

namespace IServe\Microservice\Common\Exceptions;


use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Database\QueryException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

trait DummyException
{

    protected $exceptionMaps = [
        NotFoundHttpException::class => [
            'success' => false,
            'message' => 'general.invalid_route',
            'error' => [
                'code' => 404,
            ]
        ],
        QueryException::class => [
            'success' => false,
            'message' => 'general.invalid_route',
            'error' => [
                'code' => 404,
            ],
            'adaptMessage' => true
        ],
        NotAuthorizedException::class => [
            'success' => false,
            'error' => [
                'code' => 404,
            ],
            'adaptMessage' => true
        ],
        ServerException::class => [
            'success' => false,
            'error' => [
                'code' => 404,
            ],
            'adaptMessage' => true
        ],
        RequestException::class => [
            'success' => false,
            'error' => [
                'code' => 404,
            ],
            'adaptMessage' => true
        ],
        ConnectException::class => [
            'success' => false,
            'error' => [
                'code' => 404,
            ],
            'adaptMessage' => true
        ],
        ValidationException::class => [
            'success' => false,
            'error' => [
                'code' => 404,
            ],
            'adaptMessage' => true
        ],
    ];

    /**
     * A simple implementation to help us format an exception before we render me
     *
     * @param Throwable $exception
     *
     * @return array
     */
    protected function formatException(Throwable $exception): array
    {
        # We get the class name for the exception that was raised
        $exceptionClass = get_class($exception);

        # we see if we have registered it in the mapping - if it isn't
        # we create an initial structure as an 'Internal Server Error'
        # note that this can always be revised at a later time
        $definition = $this->exceptionMaps[$exceptionClass] ?? [
                'success' => false,
                'message' => $exception->getMessage() ?? 'Something went wrong while processing your request',
                'error' => false,
            ];

        if (!empty($definition['adaptMessage'])) {
            $definition['message'] = $exception->getMessage() ?? $definition['message'];
        }

        return [
            'success' => $definition['success'],
            'message' => $definition['message'],
            'error' => $definition['error'],
        ];
    }
}
