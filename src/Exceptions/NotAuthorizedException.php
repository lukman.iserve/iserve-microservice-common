<?php

namespace IServe\Microservice\Common\Exceptions;

use Exception;

class NotAuthorizedException extends Exception
{
    public function __construct($message, $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    // Redefine the exception so message isn't optional

    public function render()
    {
        // ...
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function message()
    {
        return $this->message;
    }
}