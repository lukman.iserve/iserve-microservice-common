<?php

namespace IServe\Microservice\Common\Http\Traits;

trait ResponseTrait
{

    /**
     * Http response status code
     */
    protected $successStatus = 200;
    protected $badRequestStatus = 400;
    protected $unauthorizedStatus = 401;
    protected $notFoundStatus = 404;
    protected $unprocessableStatus = 422;

    /**
     * Generate successful response array.
     *
     * @return array()
     */
    public function requestResponses()
    {
        return [
            'success' => true,
            'message' => null,
            'response' => null,
        ];
    }

    /**
     * Generate fail response array.
     *
     * @return array()
     */
    public function requestErrors()
    {
        return [
            'success' => false,
            'message' => null,
            'error' => null
        ];
    }
}
