<?php

namespace IServe\Microservice\Common\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;


class AuthenticateAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('secret-key') == env('ACCEPTED_SECRETS')) {
            return $next($request);
        } else {
            $result['success'] = false;
            $result['message'] = trans("general.invalid.secret.key");
            $result['error'] = [
                'url' => $request->fullUrl()
            ];

            return response()->json($result);
            abort(Response::HTTP_UNAUTHORIZED);
        }
    }
}
