<?php

namespace IServe\Microservice\Common;

use Illuminate\Support\ServiceProvider;


class IServeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
       
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
