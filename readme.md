# Microservice Communication

Common library for microservice inter-communication for laravel / lumen 🚀.
All error are return from in form of json

# Features
1. Http request with Secret Key
2. Secret key validation
3. Exception handling for common HTTP use case

### Tech
1. Laravel or lumen
2. guzzlehttp/guzzle v7

### Installation
1. add snippet of ``` "repositories": [{"type": "vcs","url":"https://gitlab.com/lukman.iserve/iserve-microservice-common.git"}]```to composer file.
2. add snippet of ``` "require": {"iserve/microservice-common": "0.0.16"}```to composer file.
3. run composer install or composer update.
4. wait.
5. profit.
### How to use
| Namespace | Class | Description | Function/How to Use |
| ------ | ------ | ------ | ------ |
| IServe\Microservice\Common\Exceptions | NotAuthorizedException | an exception for invalid secret key
| IServe\Microservice\Common\Exceptions | Handler | an extension from class```Illuminate\Foundation\Exceptions\Handler``` | extend this class on handler class to inherit handling of HTTP exception 
| IServe\Microservice\Common\Http\Traits | ConsumeExternalService | perform the microservice request with secret key | 1. ```performRequest($method, $requestUrl, $formParams = [], $headers = []) ``` <br > 2. declare variable of ```$baseUri``` and ```$secret``` at calling class |
| IServe\Microservice\Common\Http\Middleware | AuthenticateAccessMiddleware | middleware for checking secret key | Lumen - include this line on bootsrap/app.php ```$app->middleware([IServe\Microservice\Common\Http\Middleware\AuthenticateAccessMiddleware::class]);``` in middleware section| 


### Example Microservice Communication
Refer example
